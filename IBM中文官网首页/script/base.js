var timer = null;


// 控制下拉菜单显示隐藏
function flagIsTrue(flag, menuSon) {
    if (flag) {
        menuSon.style.cssText += 'display:flex';
    } else {
        menuSon.style.cssText += 'display:none';
    }
};


// 防抖
function flagDelay(flag, callback, menuSon) {
    return function() {
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(callback.bind(this, flag, menuSon), 200);
    }
};


(() => {
    var hasSonMenu = document.querySelectorAll('.meun-item.have-son');
    var menuSon = document.getElementsByClassName('drop-down-content')[0];
    var flag = false;
    for (let i = 0; i < hasSonMenu.length; i++) {
        hasSonMenu[i].addEventListener('mouseenter', function() {
            menuSon.style.cssText += `background-color:rgb(${i*60},${i*60},${i*60});`;
            flag = true;
            flagDelay(flag, flagIsTrue, menuSon)();
        });
        hasSonMenu[i].addEventListener('mouseleave', function() {
            flag = false;
            flagDelay(flag, flagIsTrue, menuSon)();
        });
    }
    menuSon.addEventListener('mouseenter', function() {
        flag = true;
        flagDelay(flag, flagIsTrue, menuSon)();
    });
    menuSon.addEventListener('mouseleave', function() {
        flag = false;
        flagDelay(flag, flagIsTrue, menuSon)();
    });

})();